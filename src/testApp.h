#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxKinect.h"
//htttp
#include "ofxWebServer.h"

#include "ofxXmlSettings.h"

///#include <vector>

class testApp : public ofBaseApp , public ofxWSRequestHandler{
	public:

		void setup();
		void update();
		void draw();
		void exit();



		void keyPressed  (int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

		ofxKinect kinect;

		ofxCvColorImage		colorImg;

		ofxCvGrayscaleImage 	grayImage;
		ofxCvGrayscaleImage 	grayPrevImage;//image precedente
		ofxCvGrayscaleImage 	grayDiff;//image de la difference
		ofxCvGrayscaleImage 	grayThresh;
		ofxCvGrayscaleImage 	grayThreshFar;

		ofxCvContourFinder 	motionDetector;///pour analyser le mouvement
		ofxCvContourFinder 	PresenceFinder;///pour mesurer la presence
		ofPoint  CentrePresence;

		bool				bThreshWithOpenCV;
		bool				drawPC;

		int 				nearThreshold;
		int					farThreshold;
		int 				motionThreshold;

		int					angle;

		int 				pointCloudRotationY;


		//pour serveur http
		/*
		void getRequest(ofxHTTPServerResponse & response);
		void postRequest(ofxHTTPServerResponse & response);

        ofxHTTPServer * server;
        * */
        
        ofxWebServer server;
		void httpGet(string url);
        
        //

        //dialogues kml
        string loadfile(char* filename);
        string refresh;//fichier kml de rafraichissement
		string hud;//fichier kml de depart
        ofxXmlSettings XML;///preferences en XML

		///mouvement dans Google earth

///detection de mouvement
		ofPoint Memcentroid;///vecteur correspondant au mouvement dans l'ecran
		int nombremouvements;///quantité de mvt

		float vitesse;
		float vitesseKmh;

		///point de departs
		vector <ofPoint>  startCoords;
		vector <ofPoint>  startAtitudes;


		ofPoint coords;///coordonnees long,lat,alt
		ofPoint atitude;///heading,tilt,roll
		int TimeDelay;///temps passé entre deux pas de vol
		int plancher;
		int plafond;
		int FacteurVitesse;///facteur entre la quantité de mouvement et la vitesse /sol
		int FacteurAltitude;///inverse de l'influence de la vitesse sur l'altitude
		int FacteurAngle; ///facteur d'influence de l'angle

		void fly();///fonction pour interpretter le mouvement en déplacement dans l'espace

		void loadprefs();
        void saveprefs();
};
