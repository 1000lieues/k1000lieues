# K100LIeues
voler dans Google earth avec une Kinect
cdriko(at]free.fr
cedric Doutriaux
licence
http://creativecommons.org/licenses/by-nc-sa/2.0/fr/

## mode d'emploi
1-lancer le programme
2-ouvrir le fichier bin/data/util/lien.kml dans Google earth

en cours de fonctionnement :
- taper "a" ramène au point de départ
- pour se déplacer instantanément, appeler (de n'importe quel programme)
http://localhost:10000/go.kml?long=19.545867193278&lat=7.21527902371642&alt=1000&head=96.1104162772204&tilt=17.7709579436&roll=0




## réglages

régler le lieu de départ dans le fichier
/data/preferences.xml

(on peu s'inspirer d'un placemark généré avec Google earth)

ATTENTION : une altitude = 0 peu provoquer des erreurs de cap (mettre plutot ~ 100)




## compilation
utiliser ce projet dans codeblocks avec  openframeworks 0073
et la version ci jointe de ofxHttpServer (à décompresser dans les addons)



